using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public static Action<Texture2D> OnHover;
    public static Action OnExit;
    [SerializeField] private Texture2D cursorTexture;
 
    public void OnPointerEnter(PointerEventData eventData)
    {
        OnHover?.Invoke(cursorTexture);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnExit?.Invoke();
    }

    public void ChangeTexture(Texture2D texture2D)
    {
        cursorTexture = texture2D;
    }
}
