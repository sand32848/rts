using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class MidTextAnnoucer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI annoucerText;
    [SerializeField] private CanvasGroup canvasGroup;

    public static Action<string> onCallText;

    private void OnEnable()
    {
        onCallText += ShowText;
    }

    private void OnDisable()
    {
        onCallText -= ShowText;
    }

    public void ShowText(string text)
    {
        annoucerText.text = text;

        var sequence = DOTween.Sequence();

        sequence.Append(canvasGroup.DOFade(1, 0.1f));
        sequence.AppendInterval(1f);
        sequence.Append(canvasGroup.DOFade(0, 0.1f));
    }
}
