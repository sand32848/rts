using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Resolution : MonoBehaviour
{
    bool FullSreen = true;
    public TMP_Dropdown DD;
    public Vector2[] Resolutions;
    public void toggleFullScreen()
    {
        Debug.Log("Toggle Fullscreen, Change to " + !FullSreen);
        FullSreen = !FullSreen;
    }

    public void OnDropDownChange()
    {
        int change = DD.value;
        int X = (int)Resolutions[change].x;
        int Y = (int)Resolutions[change].y;
        Screen.SetResolution(X,Y, FullSreen);
        Debug.Log("Set resolution : " + X + "x" + Y + " ," + FullSreen);
    }
}
