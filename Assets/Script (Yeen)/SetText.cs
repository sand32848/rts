using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetText : MonoBehaviour
{
    [SerializeField] public string Name;
    [TextArea(3, 10)]
    [SerializeField] public string Dialogue;

    [Space]

    [SerializeField] public TextMeshProUGUI NameGUI;
    [SerializeField] public TextMeshProUGUI DialogueGUI;

    public void SetString()
    {
        NameGUI.text = Name;
        DialogueGUI.text = Dialogue;
    }
}
