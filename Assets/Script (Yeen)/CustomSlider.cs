using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CustomSlider : MonoBehaviour
{
    public float Value;
    public TextMeshProUGUI TextValue;
    public Slider Myslider;



    public void UpdateValue()
    {
        Value = Myslider.value * 100;
        TextValue.text = Mathf.Round(Value).ToString();
        //Play some sound as a feedback? idk
        //Set actualy mixer sound level here
    }
}
