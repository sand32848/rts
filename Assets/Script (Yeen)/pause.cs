using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class pause : MonoBehaviour
{
    public PlayerAction PA;
    private InputAction ESC;
    public GameObject Pausemenu;
    private bool Paused = false;


    private void ESc(InputAction.CallbackContext context)
    {
        togglePause();
    }

    private void Awake()
    {
        PA = new PlayerAction();
    }
    private void OnEnable()
    {
        ESC = PA.UI.Pause;
        ESC.Enable();

        ESC.performed += ESc;
    }
    private void OnDisable()
    {
        ESC.Disable();
    }

    public void togglePause()
    {
        Paused = !Paused;
        Debug.Log(gameObject.name + " Called a pause, Pause = " + Paused);

        if (Paused)
        {
            Pausemenu.SetActive(true);
        }
        else
        {
            Pausemenu.SetActive(false);
        }
    }

    public void Loadscene(int Scene)
    {
        SceneManager.LoadScene(Scene);
    }
}
