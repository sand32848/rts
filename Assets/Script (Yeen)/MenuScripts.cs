using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScripts : MonoBehaviour
{
    public void Quit()
    {
        Debug.Log("Exit");
        Application.Quit();
    }

    public void Newgame()
    {
        Debug.Log("Newgame");
        //New game
    }

    public void ResumeGame()
    {
        Debug.Log("Resume Game");

        //Load savedata?
        //Or just do somethinghere
    }

    public void loadLevel(int SceneNumber)
    {
        Debug.Log("Load Level " + SceneNumber);

        //Load savedata?

        //SceneManager.LoadScene(SceneNumber);
    }

}
