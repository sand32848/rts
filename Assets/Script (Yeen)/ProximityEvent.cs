using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProximityEvent : MonoBehaviour
{
    [SerializeField] public UnityEvent TriggerEnterEvent;
    [SerializeField] public UnityEvent TriggerExitEvent;
    [SerializeField] public string Tag;





    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("TriggerEnter: " + other.gameObject.name);

        if (other.gameObject.CompareTag(Tag))
        {
            Debug.Log(other.gameObject.name + ": Triggered");
            TriggerEnterEvent.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("TriggerExit: " + other.gameObject.name);

        if (other.gameObject.CompareTag(Tag))
        {
            TriggerExitEvent.Invoke();
        }
    }
}
