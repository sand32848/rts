using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Building : MonoBehaviour
{
    [SerializeField] private List<Renderer> outerWallRenderer;

    private void Start()
    {
        outerWallRenderer = GetComponentsInChildren<Renderer>().ToList();
    }

    public void SetVisible()
    {
        foreach (Renderer r in outerWallRenderer) 
        {
            r.enabled = !r.enabled;

            if (r.enabled == false)
            {
                r.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            }
            else
            {
                r.gameObject.layer = LayerMask.NameToLayer("Default");
            }
        }
    }
}
