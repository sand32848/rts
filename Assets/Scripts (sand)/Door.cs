using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private InventoryItemSO key;
    [SerializeField]private bool isLock = false;
    private Animator animator;
    private InventoryController inventoryController;

    private void Start()
    {
        animator = GetComponent<Animator>();
        inventoryController = GameObject.FindGameObjectWithTag("PlayerUI").GetComponent<InventoryController>();
    }

    public void CheckKey()
    {
        if (isLock)
        {
            if (inventoryController.HandleItemCheck(key))
            {
                animator.Play("Open");
            }
            else
            {
                MidTextAnnoucer.onCallText("LOCKED");
            }

        }
        else
        {
            animator.Play("Open");
        }
    }
}
