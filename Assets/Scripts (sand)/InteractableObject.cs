using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractableObject : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler,IPointerDownHandler,IPointerUpHandler
{
    [SerializeField] private ObjectSO objectSO;
    public static Action<InteractInfo> OnChooseAttack,OnChooseInteract, OnChoooseMove;
    public static Action<ObjectSO> OnInteractOption;

    private static float holdTime = 0.5f;
    private float _holdTime;
    private bool isHolding;

    private void Start()
    {
        _holdTime = holdTime;
    }

    private void Update()
    {
        if (isHolding)
        {
            _holdTime -= Time.deltaTime;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Right) return;

        InteractInfo objectInfo = new InteractInfo(gameObject, eventData.pointerCurrentRaycast.worldPosition);

        if (objectSO.GetType() == typeof(NpcSO))
        {
            NpcSO npc = (NpcSO)objectSO;
            
            if(npc.npcState == NpcState.Hostile)
            {
                
                OnChooseAttack?.Invoke(objectInfo);
            }
            else
            {
                OnChooseInteract?.Invoke(objectInfo);
            }
        }
        else if (objectSO.GetType() == typeof(ObjectSO))
        {
            OnChooseInteract?.Invoke(objectInfo);
        }
        else
        {
            OnChoooseMove?.Invoke(objectInfo);
        }

        isHolding = true;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        isHolding = false;
        _holdTime = holdTime;
    }

    public void  OnPointerEnter(PointerEventData eventData)
    {
      
    }

    public void OnPointerExit(PointerEventData eventData)
    {
  
    }
}

public struct InteractInfo
{
    public InteractInfo(GameObject target, Vector3 _clickPosition)
    {
        clickTarget = target;
        clickPosition = _clickPosition;
    }

    public GameObject clickTarget;
    public Vector3 clickPosition;
}
