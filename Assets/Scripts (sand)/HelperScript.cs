using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelperScript : MonoBehaviour
{
    public static Vector3 DirToTarget(Vector3 origin,Vector3 target)
    {
        return (target - origin).normalized;
    }

    public static GameObject GetPlayer()
    {
        return GameObject.FindGameObjectWithTag("Player");
    }

    public static Vector2 RadianToVector2(float radian)
    {
        return new Vector2(Mathf.Sin(radian), Mathf.Cos(radian));
    }

    public static Vector2 DegreeToVector2(float degree)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }

    public static float Vector2ToDegre(Vector2 vector)
    {
        if (vector.x < 0)
        {
            return 360 - (Mathf.Atan2(vector.x, vector.y) * Mathf.Rad2Deg * -1);
        }
        else
        {
            return Mathf.Atan2(vector.x, vector.y) * Mathf.Rad2Deg;
        }
    }

    public static Vector2 DirToPlayer(Vector2 origin)
    {
        return ((Vector2)GetPlayer().transform.position - origin).normalized;
    }

    public static Vector2 DirToMouse(Vector2 origin)
    {
        return ((Vector2)Camera.main.ScreenToWorldPoint(InputController.Instance.PlayerAction.Player.MousePos.ReadValue<Vector2>()) - origin).normalized;
    }
}
