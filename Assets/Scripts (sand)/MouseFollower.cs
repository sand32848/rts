using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollower : MonoBehaviour
{
    [SerializeField]
    private Canvas canvas;

    private Camera cam;

    [SerializeField]
    private InventorySlot item;

    public void Awake()
    {
        cam = Camera.main;
        canvas = transform.root.GetComponent<Canvas>();
        item = GetComponentInChildren<InventorySlot>();
    }

    public void SetData(Sprite sprite, int amount)
    {
        item.SetData(sprite, amount);
    }

    void Update()
    {
        Vector2 position;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            (RectTransform)canvas.transform,
            Input.mousePosition,
            canvas.worldCamera,
            out position
                );
        transform.position = canvas.transform.TransformPoint(position);
    }

    public void Toggle(bool val)
    {
        gameObject.SetActive(val);
    }
}
