using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PickUp : MonoBehaviour
{
    [SerializeField] private InventoryItemSO item;
    public static Action<InventoryItemSO> OnItemPickup;

    private void OnMouseDown()
    {
        OnItemPickup?.Invoke(item);
    }


}
