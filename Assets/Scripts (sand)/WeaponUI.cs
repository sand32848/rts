using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI weaponName;
    [SerializeField] private TextMeshProUGUI weaponAmmo;
    [SerializeField] private Image weaponIcon;
    private void OnEnable()
    {
        WeaponManager.OnChangeWeapon += UpdateWeaponUI;
        AttackCommand.OnParamChange += UpdateWeaponUI;
        InventorySO.OnWeaponParamChange += UpdateWeaponUI;


    }

    private void OnDisable()
    {
        WeaponManager.OnChangeWeapon -= UpdateWeaponUI;
        AttackCommand.OnParamChange-= UpdateWeaponUI;
        InventorySO.OnWeaponParamChange -= UpdateWeaponUI;
    }

    public void UpdateWeaponUI(InventoryItem invWeapon)
    {
        weaponIcon.sprite = invWeapon.item.icon;
        weaponName.text = invWeapon.item.name;
        weaponAmmo.text = invWeapon.GetParameter("AmmoCount").value.ToString("0") +"/"+ invWeapon.GetParameter("MagSize").value.ToString("0"); ;
    }
}
