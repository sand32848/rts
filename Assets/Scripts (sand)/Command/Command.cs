using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Command 
{
    public abstract bool IsFinish { get; }
    public abstract void Execute();
    public abstract void OnCommandBegin();
    public abstract void OnCommandEnd();
}
