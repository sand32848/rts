using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.AI;

public class AttackCommand : Command
{
    private GameObject target;
    private GameObject attacker;
    private NavMeshAgent navMesh;
    private AttackController attackController;
    private Health targetHealth;
    private float _fireRate;
    private InventorySO inventoryHelper;
    private List<ItemParameter> parametertoModify;
    private List<ItemParameter> itemCurrentstate;
    private WeaponSO weapon;
    private InventoryItem currentInventoryItem;

    public static Action<InventoryItem> OnParamChange;

    public AttackCommand(GameObject attacker,GameObject target, NavMeshAgent navmesh,AttackController attackController,InventorySO inventorySO = null)
    {
        this.navMesh = navmesh;
        this.target = target;
        this.attackController = attackController;
        this.targetHealth = attackController.currentTarget.GetComponent<Health>();
        this.attacker = attacker;
        this.inventoryHelper = inventorySO;
        this.itemCurrentstate = attackController.currentWeapon.itemState;
        this.currentInventoryItem = attackController.currentWeapon;
        this.parametertoModify = new List<ItemParameter>(attackController.parametertToModify);
        weapon = (WeaponSO)attackController.currentWeapon.item;
        _fireRate = weapon.fireRate;
    }

    public override bool IsFinish => targetHealth.health <= 0;

    public override void Execute()
    {
        _fireRate -= Time.deltaTime;

        RaycastHit hit;
        if (Physics.Raycast(attacker.transform.position, HelperScript.DirToTarget(attacker.transform.position, target.transform.position), out hit, weapon.Range))
        {
            if (hit.transform.gameObject != target)
            {
                navMesh.SetDestination(target.transform.position);
                navMesh.stoppingDistance = weapon.Range;
                return;
            }

            if (_fireRate <= 0)
            {
                Attack();
                _fireRate = weapon.fireRate;
            }
        }
        else
        {
            navMesh.SetDestination(target.transform.position);
            navMesh.stoppingDistance = weapon.Range;
        }
    }

    public void Attack()
    {
        if(inventoryHelper) ModifyParameter();

        if (attackController.currentTarget.TryGetComponent(out Health health))
        {
            for (int i = 0; i < weapon.burstAmount; i++)
            {
                health.ReduceHealth(weapon.damage);
            }
        }
    }

    public void ModifyParameter()
    {
        foreach (var parameter in parametertoModify)
        {
            if (itemCurrentstate.Contains(parameter))
            {
                int index = itemCurrentstate.IndexOf(parameter);
                float newValue = itemCurrentstate[index].value + parameter.value;
                itemCurrentstate[index] = new ItemParameter
                {
                    itemParameter =  parameter.itemParameter,
                    value = newValue,
                };
            }
        }

        if (inventoryHelper)
        {
            OnParamChange?.Invoke(currentInventoryItem);
        }
    }

    public override void OnCommandBegin()
    {

    }

    public override void OnCommandEnd()
    {

    }
}
