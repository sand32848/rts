
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class CommandManager : MonoBehaviour
{
    public static Action OnCancle;

    private Command currentCommand;
    public Queue<Command> commands= new Queue<Command>();

    private void Update()
    {
        CheckForCommand();
        ProcessCommand();
    }

    public void AddCommand(Command command)
    {
        OnCancle?.Invoke();
        commands.Clear();

        if (currentCommand != null)
        {
            currentCommand.OnCommandEnd();
            currentCommand = null;
        }
       

        currentCommand = command;
        currentCommand.OnCommandBegin();
    }

    public void CancleCommand(InputAction.CallbackContext context)
    {
        OnCancle?.Invoke();
        commands.Clear();
    }

    public void ProcessCommand()
    {
        if (currentCommand == null) return;
        currentCommand.Execute();
    }

    public void CheckForCommand()
    {
        if (currentCommand == null) return;

        if (currentCommand.IsFinish)
        {
            currentCommand.OnCommandEnd();

            if(commands.Any() == false)
            {
                currentCommand = null;
            }
            else
            {
                currentCommand = commands.Dequeue();
                currentCommand.OnCommandBegin();
            }
        }
    }
}
