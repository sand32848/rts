using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveCommand : Command
{
    private NavMeshAgent navMesh;
    private Vector3 targetPosition;

    public MoveCommand( Vector3 targetPosition, NavMeshAgent navmesh)
    {
        this.navMesh = navmesh;
        this.targetPosition = targetPosition;   
    }
    public override bool IsFinish => navMesh.remainingDistance <= 0.1;

    public override void Execute()
    {

    }

    public override void OnCommandBegin()
    {
        navMesh.SetDestination(targetPosition);
        navMesh.stoppingDistance = 0;
    }

    public override void OnCommandEnd()
    {
      
    }


}
