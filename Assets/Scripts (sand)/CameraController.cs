using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float zoomAmount;
    [SerializeField] private float minFov, maxFov;
    [SerializeField] private float maxLook, minLook;

    [SerializeField] private float snapDistance;
    [SerializeField] private float smoothTime;

    private bool toggleCamRotate;

    private Vector3 originHeight;
    private Vector2 moveDir = Vector2.zero;
    private Vector2 mouseDir = Vector2.zero;

    private Vector3 currentRotation;
    private Vector3 smoothVelocity;
    private float rotatationX;
    private float rotatationY;

    private void Start()
    {
        rotatationX = transform.localEulerAngles.x;
        rotatationY = transform.localEulerAngles.y;
    }

    private void OnEnable()
    {
        SubFunc();
    }

    private void OnDisable()
    {
        UnsubFunc();
    }

    private void Update()
    {
        Vector3 camMoveDir = Vector3.Cross(transform.right,Vector3.up);

        transform.position += camMoveDir * moveDir.y * moveSpeed * Time.deltaTime;
        transform.position += transform.right * moveDir.x * moveSpeed * Time.deltaTime;
    }

    public void Move(InputAction.CallbackContext ctx)
    {
       moveDir =  ctx.ReadValue<Vector2>();
    }

    public void RotateCam(InputAction.CallbackContext ctx)
    {
        toggleCamRotate = ctx.ReadValueAsButton();
    }

    public void Zoom(InputAction.CallbackContext ctx)
    {
        float zoom = ctx.ReadValue<float>() == 120 ? -zoomAmount : zoomAmount;
        camera.fieldOfView += zoom;

        camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, minFov, maxFov);
    }

    public void Rotate(InputAction.CallbackContext ctx)
    {
        mouseDir = ctx.ReadValue<Vector2>();

        if (toggleCamRotate)
        {
            float mouseX = mouseDir.x * rotateSpeed;
            float mouseY = -mouseDir.y * rotateSpeed;

            rotatationY += mouseX;
            rotatationX += mouseY;

            rotatationX = Mathf.Clamp(rotatationX, maxLook, minLook);

            Vector3 nextRotation = new Vector3(rotatationX, rotatationY);
            currentRotation = Vector3.SmoothDamp(currentRotation, nextRotation,ref smoothVelocity,smoothTime);

            transform.localEulerAngles = currentRotation;
        }
    }

    public void FocusCam(GameObject focusGameobject)
    {
        transform.position = focusGameobject.transform.position - transform.forward * snapDistance;
    }

    public void UnsubFunc()
    {
        InputController.Instance.PlayerAction.Player.Zoom.started -= Zoom;

        InputController.Instance.PlayerAction.Player.RotateCam.started += RotateCam;
        InputController.Instance.PlayerAction.Player.RotateCam.canceled += RotateCam;

        InputController.Instance.PlayerAction.Player.Move.performed -= Move;
        InputController.Instance.PlayerAction.Player.Move.canceled -= Move;

        InputController.Instance.PlayerAction.Player.MouseMovement.performed -= Rotate;
    }

    public void SubFunc()
    {
        InputController.Instance.PlayerAction.Player.Zoom.started += Zoom;

        InputController.Instance.PlayerAction.Player.RotateCam.started += RotateCam;
        InputController.Instance.PlayerAction.Player.RotateCam.canceled += RotateCam;

        InputController.Instance.PlayerAction.Player.Move.performed += Move;
        InputController.Instance.PlayerAction.Player.Move.canceled += Move;

        InputController.Instance.PlayerAction.Player.MouseMovement.performed += Rotate;

    }
}
