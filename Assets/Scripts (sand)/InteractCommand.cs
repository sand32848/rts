using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using UnityEngine;
using UnityEngine.AI;

public class InteractCommand : Command
{
    private GameObject target;
    private GameObject interacter;
    private NavMeshAgent navMesh;

    private float interactRange = 2;

    private bool finish;

    public static Action<InventoryItem> OnParamChange;

    public InteractCommand(GameObject interacter, GameObject target, NavMeshAgent navmesh)
    {
        this.navMesh = navmesh;
        this.target = target;
        this.interacter = interacter;
    }

    public override bool IsFinish => finish;

    public override void Execute()
    {
        RaycastHit hit;
        if (Physics.Raycast(interacter.transform.position, HelperScript.DirToTarget(interacter.transform.position, target.transform.position), out hit, interactRange))
        {
            if (hit.transform.gameObject == target)
            {
                target.GetComponent<InteractEvent>().onInteract.Invoke();
                finish = true;
                return;
            }
        }
        else
        {
            navMesh.SetDestination(target.transform.position);
            navMesh.stoppingDistance = interactRange;
        }
    }

    public override void OnCommandBegin()
    {

    }

    public override void OnCommandEnd()
    {

    }
}
