using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClickInfo : MonoBehaviour
{
    private MovementController movementController;
    private AttackController attackController;
    private InteractController interactController;

    private void Start()
    {
        movementController = GetComponent<MovementController>();
        attackController = GetComponent<AttackController>();
        interactController = GetComponent<InteractController>();
    }

    private void OnEnable()
    {
        InteractableObject.OnChooseAttack += Attack;
        InteractableObject.OnChoooseMove += Move;
        InteractableObject.OnChooseInteract += Interact;
    }

    private void OnDisable()
    {
        InteractableObject.OnChooseAttack -= Attack;
        InteractableObject.OnChoooseMove -= Move;
        InteractableObject.OnChooseInteract -= Interact;

    }

    public void Attack(InteractInfo interactInfo)
    {
        attackController.SetTarget(interactInfo.clickTarget);
    }

    public void Move(InteractInfo interactInfo)
    {
        movementController.MoveTo(interactInfo.clickPosition);

    }

    public void Interact(InteractInfo interactInfo)
    {
        interactController.SetTarget(interactInfo.clickTarget);
    }
}
