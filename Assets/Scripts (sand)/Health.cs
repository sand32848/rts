using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [field:SerializeField] public float health { get; protected set; }
    [field: SerializeField] public float maxhealth { get; protected set; }

    public Action OnDeath;
    public Action OnTakeDamage;

    public virtual void ReduceHealth(float damage)
    {
        health -= damage;

        OnTakeDamage?.Invoke();

        if(health <= 0)
        {
            OnDeath?.Invoke();
        }
    }
}
