using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    public static Action<float,float> OnHealthChange;

    private void Start()
    {
        OnHealthChange?.Invoke(health, maxhealth);
    }
    public override void ReduceHealth(float damage)
    {
        health -= damage;

        OnHealthChange?.Invoke(health,maxhealth);

        if (health <= 0)
        {
            OnDeath?.Invoke();
        }
    }
}
