using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackController : MonoBehaviour
{

    public GameObject currentTarget { get; private set; }
    [field:SerializeField] public InventoryItem currentWeapon { get; private set; }
    private CommandManager commandManager;
    private NavMeshAgent navMesh;
    [SerializeField] private InventorySO inventoryHelper;
    [field: SerializeField] public List<ItemParameter> parametertToModify { get; private set; } = new List<ItemParameter>();

    public float _fireRate { get; set; }

    private void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        commandManager= GetComponent<CommandManager>();   
    }

    public void SetTarget(GameObject target)
    {
        if (currentWeapon.IsEmpty) return;
        currentTarget = target;
        AttackCommand attackCommand = new AttackCommand(gameObject,currentTarget, navMesh, this,inventoryHelper);
        commandManager.AddCommand(attackCommand);
    }

    public void SetWeapon(InventoryItem inventoryWeapon)
    {
        currentWeapon = inventoryWeapon;

        _fireRate = 0;
    }

    public void CancleAttack()
    {
        currentTarget = null;
        _fireRate = 0;
    }

}
