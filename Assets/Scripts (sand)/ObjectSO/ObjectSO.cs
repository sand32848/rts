using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Object", menuName = "Object/Object")]
public class ObjectSO : ScriptableObject
{
    public InteractType interactType; 
}

public enum InteractType { Npc, Item, Environment, Nothing }