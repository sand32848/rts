using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NPC", menuName = "Object/NPC")]
public class NpcSO : ObjectSO
{
    public NpcState npcState;
    public int Health;
}
public enum NpcState { Neutral, Hostile, Friendly };