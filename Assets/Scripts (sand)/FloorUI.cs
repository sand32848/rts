using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FloorUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI floorText;

    private void OnEnable()
    {
        FloorManager.OnChangeFloor += UpdateFloorUI;
    }

    private void OnDisable()
    {
        FloorManager.OnChangeFloor -= UpdateFloorUI;

    }

    private void UpdateFloorUI(int floorIndex)
    {
        floorText.text = (floorIndex + 1).ToString();
    }
}
