using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorObject : MonoBehaviour
{
    [field: SerializeField] public int floorIndex { get; private set; }
    private Renderer renderer;

    private string originalLayer;

    private void Awake()
    {
        renderer = GetComponent<Renderer>();
        originalLayer =  LayerMask.LayerToName(gameObject.layer);
    }

    private void Start()
    {
        //print(gameObject.layer + gameObject.name);
    }

    private void OnEnable()
    {
        FloorManager.OnChangeFloor += CheckFloor;
    }

    private void OnDisable()
    {
        FloorManager.OnChangeFloor -= CheckFloor;
    }

    public void ChangeFloor(int floor)
    {
        floorIndex = floor;
    }

    //check what the current floor is and disable renderer accordingly
    public void CheckFloor(int _floorIndex)
    {
        int LayerIgnoreRaycast;
        if (_floorIndex < floorIndex)
        {
            renderer.enabled = false;
             LayerIgnoreRaycast = LayerMask.NameToLayer("Ignore Raycast");
      
        }
        else
        {
            LayerIgnoreRaycast = LayerMask.NameToLayer(originalLayer);
            renderer.enabled = true;
        }
        gameObject.layer = LayerIgnoreRaycast;
    }
}
    
