using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;
using UnityEngine.AI;

public class Detection : MonoBehaviour
{
    [SerializeField] private float range;
    private AttackController attackController;
    private Action OnDetect;

    private bool Detected;

    private void Start()
    {
        attackController = GetComponent<AttackController>();
    }
    void Update()
    {
        if (Detected) return;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, HelperScript.DirToTarget(transform.position, HelperScript.GetPlayer().transform.position), out hit,range))
        {
            attackController.SetTarget(HelperScript.GetPlayer());
            Detected = true;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
