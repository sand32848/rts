using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class InteractController : MonoBehaviour
{
    public GameObject currentTarget { get; private set; }
    private CommandManager commandManager;
    private NavMeshAgent navMesh;
    [SerializeField] private InventorySO inventoryHelper;

    public float _fireRate { get; set; }

    private void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        commandManager = GetComponent<CommandManager>();
    }

    public void SetTarget(GameObject target)
    {
        currentTarget = target;
        InteractCommand interactCommand = new InteractCommand(gameObject, currentTarget, navMesh);
        commandManager.AddCommand(interactCommand);
    }
}
