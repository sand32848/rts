using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Transform PivotFocus;
    [SerializeField] private GameObject tetherObject;
    [SerializeField] private float distanceToTarget = 10;
    [SerializeField] private float moveSpeed;

    [SerializeField] private float zoomAmount;

    [SerializeField] private float maxDistance;
    [SerializeField] private float minDistance;

    [SerializeField] private float maxYRotation;
    [SerializeField] private float minYRotation;
    [SerializeField] private float currentZoom;

    [SerializeField] private float tetherRange;

    private Vector2 moveDir = Vector2.zero;

    private Vector3 localRot;

    [SerializeField] private float damping;

    private void Start()
    {
        currentZoom = (PivotFocus.transform.position - cam.transform.position).magnitude;
        localRot = transform.rotation.eulerAngles;
    }


    public void TetherCheck()
    {
        Vector3 newLocation = new Vector3(PivotFocus.transform.position.x, 0, PivotFocus.transform.position.z);
        Vector3 centerLocation = new Vector3(tetherObject.transform.position.x, 0, tetherObject.transform.position.z);

        float distance = Vector3.Distance(newLocation, centerLocation); //distance from ~green object~ to *black circle*

        if (distance > tetherRange) //If the distance is less than the radius, it is already within the circle.
        {
            Vector3 fromOriginToObject = newLocation - centerLocation; //~GreenPosition~ - *BlackCenter*
            fromOriginToObject *= tetherRange / distance; //Multiply by radius //Divide by Distance
            newLocation = centerLocation + fromOriginToObject; //*BlackCenter* + all that Math

            PivotFocus.transform.position = new Vector3(newLocation.x,PivotFocus.transform.position.y,newLocation.z) ;
        }
    }

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Zoom.started += Zoom;
        InputController.Instance.PlayerAction.Player.Move.performed += Move;
        InputController.Instance.PlayerAction.Player.Move.canceled += Move;
        TogglePlayerButton.OnFocusPlayer += FocusCam;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Zoom.started -= Zoom;
        InputController.Instance.PlayerAction.Player.Move.performed -= Move;
        InputController.Instance.PlayerAction.Player.Move.canceled -= Move;
        TogglePlayerButton.OnFocusPlayer -= FocusCam;

    }

    void Update()
    {
        transform.position = PivotFocus.position;

        if (Input.GetMouseButton(2))
        {
            localRot.x += Input.GetAxis("Mouse X");
            localRot.y -= Input.GetAxis("Mouse Y");

            localRot.y = Mathf.Clamp(localRot.y, minYRotation, maxYRotation);

            Quaternion quaternion = Quaternion.Euler(localRot.y, localRot.x, 0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, quaternion, Time.deltaTime * damping);
        }

        cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, transform.InverseTransformDirection((cam.transform.forward * -1) * currentZoom), Time.deltaTime * 3);

        Vector3 camMoveDir = Vector3.Cross(transform.right, Vector3.up);

        PivotFocus.transform.position += camMoveDir * moveDir.y * moveSpeed * Time.deltaTime;
        PivotFocus.transform.position += transform.right * moveDir.x * moveSpeed * Time.deltaTime;

        TetherCheck();
    }

    public void Zoom(InputAction.CallbackContext ctx)
    {

        float zoom = ctx.ReadValue<float>() == 120 ? -zoomAmount : zoomAmount;

        currentZoom += zoom;

        currentZoom = Mathf.Clamp(currentZoom, minDistance, maxDistance);
    }

    public void Move(InputAction.CallbackContext ctx)
    {
        moveDir = ctx.ReadValue<Vector2>();
    }

    public void FocusCam(GameObject gameObject)
    {
        PivotFocus.position = gameObject.transform.position;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if(tetherObject)Gizmos.DrawWireSphere(tetherObject.transform.position, tetherRange);
    }

}
