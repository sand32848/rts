using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class MovementController : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private CommandManager interactManager;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        interactManager = GetComponent<CommandManager>();
    }


    public void MoveTo(Vector3 position)
    {

        MoveCommand moveCommand = new MoveCommand(position, navMeshAgent);
        interactManager.AddCommand(moveCommand);

    }
}
