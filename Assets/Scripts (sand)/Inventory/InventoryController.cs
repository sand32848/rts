using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    [SerializeField] private InventoryUI inventoryUI;
    [SerializeField] private InventorySO inventoryData;

    public static Action<Dictionary<int, InventoryItem>> OnWeaponChange;
    private void OnEnable()
    {
        PickUp.OnItemPickup += HandlePickup;
        ReloadManager.OnWeaponReload += HandleReload;
        inventoryData.OnChange += HandleChange;
        inventoryUI.OnSwapItem += HandleSwap;
        inventoryUI.OnRightClickItem += HandleRightClick;
        InputController.Instance.PlayerAction.UI.Inventory.started += ToggleInventory;
        InputController.Instance.PlayerAction.UI.Pause.started += CloseInventory;
    }

    private void OnDisable()
    {
        PickUp.OnItemPickup -= HandlePickup;
        ReloadManager.OnWeaponReload -= HandleReload;
        inventoryData.OnChange -= HandleChange;
        inventoryUI.OnSwapItem -= HandleSwap;
        inventoryUI.OnRightClickItem -= HandleRightClick;
        InputController.Instance.PlayerAction.UI.Inventory.started -= ToggleInventory;
        InputController.Instance.PlayerAction.UI.Pause.started -= CloseInventory;
    }

    private void Start()
    {
        inventoryUI.InitializeUI();
        inventoryData.Initialize(inventoryUI.inventorySlots);
    }

    public void HandleReload(InventoryItem currentWeapon)
    {
        inventoryData.ReloadWeapon( currentWeapon);
    }

    public void HandleAddItem(InventoryItemSO item)
    {
        inventoryData.AddItem(item, item.amount);
    }

    public void HandlePickup(InventoryItemSO item)
    {
        inventoryData.AddItem(item, item.amount);
    }

    public void HandleSwap(int itemIndex_1,int itemIndex_2,InventorySlot slot_1,InventorySlot slot_2)
    {
        inventoryData.SwapItems(itemIndex_1, itemIndex_2,slot_1,slot_2);
    }

    public void HandleRightClick(InventorySlot slot)
    {
        inventoryData.AutoEquip(slot);
    }

    public void HandleChange(Dictionary<int, InventoryItem> inventoryState)
    {
        inventoryUI.ResetAllItem();
        foreach (var item in inventoryState)
        {
            inventoryUI.UpdateData(item.Key, item.Value.item.icon,
                item.Value.amount);
        }

        OnWeaponChange?.Invoke(inventoryState);
    }
    public void ToggleInventory(InputAction.CallbackContext context)
    {
        inventoryUI.invUI.SetActive(!inventoryUI.invUI.activeSelf);
    }

    public void CloseInventory(InputAction.CallbackContext context)
    {
        inventoryUI.invUI.SetActive(false);
    }

    public bool HandleItemCheck(InventoryItemSO item)
    {
        return inventoryData.CheckItemExsist(item);
    }
}
