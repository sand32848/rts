using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] private GameObject slotHolder;
    [SerializeField] private MouseFollower mouseFollower;
    [field:SerializeField] public GameObject invUI { get;private set; }
    public List<InventorySlot> inventorySlots { get; private set; } = new List<InventorySlot>();
    public Action<int, int,InventorySlot,InventorySlot> OnSwapItem;
    public Action<InventorySlot> OnRightClickItem;
    private int currentlyDraggedItemIndex = -1;

    private void Awake()
    {
        mouseFollower.Toggle(false);
    }

    public void ResetAllItem()
    {
        foreach (var item in inventorySlots)
        {
            item.ResetData();
        }
    }

    public void InitializeUI()
    {
        InventorySlot[] inv = slotHolder.transform.GetComponentsInChildren<InventorySlot>();
        for (int i = 0; i < inv.Length; i++)
        {
            inventorySlots.Add(inv[i]);
            inv[i].index = i;
        }

        for (int i = 0; i < inventorySlots.Count; i++)
        {
            InventorySlot inventorySlot = inventorySlots[i];
            inventorySlot.OnItemBeingDrag += HandleBeginDrag;
            inventorySlot.OnItemEndDrag += HandleEndDrag;
            inventorySlot.OnItemClick += HandleItemClick;
            inventorySlot.OnItemRightClick += HandleRightClick;
            inventorySlot.OnItemDrop += HandleSwap;
        }
    }

    public void UpdateData(int itemIndex,Sprite itemImage, int itemQuantity)
    {
        if (inventorySlots.Count > itemIndex)
        {
            inventorySlots[itemIndex].SetData(itemImage, itemQuantity);
        }
    }

    public void HandleSwap(InventorySlot ToSlot)
    {
        if (currentlyDraggedItemIndex == -1) return;

        int index = inventorySlots.IndexOf(ToSlot);
        InventorySlot fromSlot = inventorySlots[currentlyDraggedItemIndex];
        if (index == -1) return;
    
        OnSwapItem?.Invoke(currentlyDraggedItemIndex,index,fromSlot,ToSlot);
    }

    public void HandleRightClick(InventorySlot slot)
    {
        OnRightClickItem?.Invoke(slot);
    }

    public void HandleItemClick(InventorySlot slot)
    {

    }

    public void HandleBeginDrag(InventorySlot slot)
    {
        if (slot.empty) return;

        mouseFollower.Toggle(true);

        int index = inventorySlots.IndexOf(slot);
        if (index == -1) return;

        currentlyDraggedItemIndex = index;

    }

    public void HandleEndDrag(InventorySlot slot)
    {
        mouseFollower.Toggle(false);
        ResetDragItem();

    }

    public void ResetDragItem()
    {
        mouseFollower.Toggle(false);
        currentlyDraggedItemIndex = -1;
    }


}
