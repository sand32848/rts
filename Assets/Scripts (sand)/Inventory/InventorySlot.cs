
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public TextMeshProUGUI Amount;
    public Image Icon;
    public int index;
    public bool empty { get; private set; } = true;
    public InventoryItemSO invType;

    public Action<InventorySlot> OnItemClick, OnItemBeingDrag, OnItemEndDrag, OnItemDrop, OnItemHover,OnItemRightClick;


    public void ResetData()
    {
        Icon.gameObject.SetActive(false);
        Amount.text = "";
        empty = true;
    }

    public void SetData(Sprite sprite, int quantity)
    {
        Icon.gameObject.SetActive(true);
        Icon.sprite = sprite;
        Amount.text = quantity + "";
        empty = false;
    }

    public void OnDrop()
    {

        OnItemDrop?.Invoke(this);

    }

    public void OnBeginDrag()
    {

        OnItemBeingDrag?.Invoke(this);

    }

    public void OnEndDrag()
    {

        OnItemEndDrag?.Invoke(this);

    }

    public void OnClick(BaseEventData data)
    {
        PointerEventData pointerData = (PointerEventData)data;
        if (pointerData.button == PointerEventData.InputButton.Right)
        {
            OnItemRightClick?.Invoke(this);
        }
        else
        {
            OnItemClick?.Invoke(this);
        }

    }

 
}

