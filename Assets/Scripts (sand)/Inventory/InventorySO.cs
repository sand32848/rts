using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using static UnityEditor.Progress;

[CreateAssetMenu(fileName = "Inventory", menuName = "Item/Inventory")]
public class InventorySO : ScriptableObject
{
    [SerializeField] private List<InventoryItem> inventoryItems = new List<InventoryItem>();
    public Action<Dictionary<int, InventoryItem>> OnChange;

    public Action<InventoryItemSO> OnWeaponChange, OnThrowableChange, OnArmorChange;
    public static Action<InventoryItem> OnWeaponParamChange;

    public void Initialize(List<InventorySlot> inventorySlots)
    {
        inventoryItems.Clear();
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            inventoryItems.Add(new InventoryItem(0, null, inventorySlots[i].invType));
        }
    }

    public void AddItem(InventoryItemSO item,int quantity)
    {
        Debug.Log(item.GetType());
        for(int i = 0; i< inventoryItems.Count; i++)
        {
            if (inventoryItems[i].invType.GetType() != typeof(InventoryItemSO)) continue;

            if (inventoryItems[i].IsEmpty)
            {
                inventoryItems[i] = new InventoryItem
                {
                    item = item,
                    amount = quantity,
                    invType = inventoryItems[i].invType,
                    itemState = new List<ItemParameter>(item.DefaultParametersList) 
                };

                InformChange();

                return;
            }
        }
    }

    public void SwapItems(int itemIndex_1, int itemIndex_2,InventorySlot slot_1,InventorySlot slot_2)
    {
        if (inventoryItems[itemIndex_1].item == inventoryItems[itemIndex_2].item && inventoryItems[itemIndex_1].item.stackable) { StackItem(inventoryItems[itemIndex_1], inventoryItems[itemIndex_2]); return; }

        if (!inventoryItems[itemIndex_1].item) return ;
        if (!CheckItemType(itemIndex_1, itemIndex_2, slot_1, slot_2)) { Debug.Log("FalseType"); return; }

        InventoryItem item1 = new InventoryItem(inventoryItems[itemIndex_1].amount, inventoryItems[itemIndex_1].item, inventoryItems[itemIndex_2].invType, inventoryItems[itemIndex_1].itemState);
        inventoryItems[itemIndex_1] = new InventoryItem(inventoryItems[itemIndex_2].amount, inventoryItems[itemIndex_2].item, inventoryItems[itemIndex_1].invType, inventoryItems[itemIndex_2].itemState);
        inventoryItems[itemIndex_2] = item1;
        InformChange();
    }

    public void StackItem(InventoryItem sender, InventoryItem reciever)
    {
        if (reciever.amount >= reciever.item.maxStack) return;
        
        int toFill = reciever.item.maxStack - reciever.amount;

        if (toFill >= sender.amount)
        {
            int newValue = reciever.amount + sender.amount;
            inventoryItems[inventoryItems.IndexOf(reciever)] = new InventoryItem(newValue, reciever.item, inventoryItems[inventoryItems.IndexOf(reciever)].invType);
            inventoryItems[inventoryItems.IndexOf(sender)] = new InventoryItem(0, null, inventoryItems[inventoryItems.IndexOf(sender)].invType);
        }
        else if(toFill < sender.amount)
        {
           int RecieverValue = reciever.amount + toFill;
           int SenderValue =  sender.amount - toFill;

            inventoryItems[inventoryItems.IndexOf(reciever)] = new InventoryItem(RecieverValue, reciever.item, inventoryItems[inventoryItems.IndexOf(reciever)].invType);
            inventoryItems[inventoryItems.IndexOf(sender)] = new InventoryItem(SenderValue, sender.item, inventoryItems[inventoryItems.IndexOf(sender)].invType);
        }

        InformChange();

    }

    public void ReloadWeapon(InventoryItem inventoryItem)
    {
        if (inventoryItem.IsEmpty) return;
        WeaponSO weapon = (WeaponSO)inventoryItem.item;

        float bulletNeed = inventoryItem.GetParameter("MagSize").value - inventoryItem.GetParameter("AmmoCount").value;
        int bulletTotal = 0;

        for(int i = 0; i< inventoryItems.Count; i++)
        {
            if (inventoryItems[i].item == weapon.ammoType)
            {
                if(bulletNeed >= inventoryItems[i].item.amount)
                {
                    bulletNeed -= inventoryItems[i].item.amount;
                    bulletTotal += inventoryItems[i].item.amount;
                    inventoryItems[i] = new InventoryItem(0, null, inventoryItems[i].invType);
                }
                else
                {
                    bulletTotal += (int)bulletNeed; 
      
                    inventoryItems[i] = new InventoryItem(inventoryItems[i].amount - (int)bulletNeed, inventoryItems[i].item, inventoryItems[i].invType);

                    bulletNeed = 0;

                }
            }

            if(bulletNeed == 0) break;
        }

        inventoryItem.itemState[inventoryItem.GetParameterIndex("AmmoCount")] = new ItemParameter(inventoryItem.GetParameter("AmmoCount").itemParameter, inventoryItem.GetParameter("AmmoCount").value+ bulletTotal);

        OnWeaponParamChange?.Invoke(inventoryItem);
        InformChange();
    }

    public bool CheckItemType(int itemIndex_1, int itemIndex_2, InventorySlot slot_1, InventorySlot slot_2)
    {
        //empty slot swap
        if (inventoryItems[itemIndex_2].item == null)
        {
            if (inventoryItems[itemIndex_1].item.GetType() == typeof(SecondaryWeaponSO) && (slot_2.invType.GetType() == typeof(SecondaryWeaponSO) || slot_2.invType.GetType() == typeof(WeaponSO))) return true;
            if (inventoryItems[itemIndex_1].item.GetType() == typeof(InventoryItemSO) && (slot_2.invType.GetType() == typeof(InventoryItemSO))) return true;
            if (inventoryItems[itemIndex_1].item.GetType() != typeof(InventoryItemSO) && (inventoryItems[itemIndex_1].item.GetType() == slot_2.invType.GetType() || slot_2.invType.GetType() == typeof(InventoryItemSO))) return true;

            return false;
        }
        else
        {
            //both secondary
            if (inventoryItems[itemIndex_1].item.GetType() == typeof(SecondaryWeaponSO) && inventoryItems[itemIndex_2].item.GetType() == typeof(SecondaryWeaponSO)) return true;

            //primary second inslot swap
            if (inventoryItems[itemIndex_1].item.GetType() == typeof(SecondaryWeaponSO) &&
                (inventoryItems[itemIndex_2].item.GetType() == typeof(WeaponSO) && slot_1.invType.GetType() == typeof(WeaponSO))) return true;

            if (inventoryItems[itemIndex_2].item.GetType() == typeof(SecondaryWeaponSO) &&
                  (inventoryItems[itemIndex_1].item.GetType() == typeof(WeaponSO) && slot_2.invType.GetType() == typeof(WeaponSO))) return true;

            //item to equip
            if (slot_2.invType.GetType() != typeof(InventoryItemSO))
            {
                if (inventoryItems[itemIndex_1].item.GetType() == typeof(InventoryItemSO))
                {
                    return false;
                }
            }

            if (inventoryItems[itemIndex_1].item.GetType() == typeof(SecondaryWeaponSO) && (slot_2.invType.GetType() == typeof(SecondaryWeaponSO) || slot_2.invType.GetType() == typeof(WeaponSO))) return true;
            
             if (inventoryItems[itemIndex_2].item.GetType() == typeof(SecondaryWeaponSO) && (slot_1.invType.GetType() == typeof(SecondaryWeaponSO) || slot_1.invType.GetType() == typeof(WeaponSO))) return true;


            // item to item
            if (slot_1.invType.GetType() == typeof(InventoryItemSO) && slot_2.invType.GetType() == typeof(InventoryItemSO)) return true;
        }

        return false;
    }

    public void AutoEquip(InventorySlot slot)
    { 
        if (inventoryItems[slot.index].item == null) return;

        //auto deequip
        if(slot.invType.GetType() != typeof(InventoryItemSO))
        {
            Debug.Log("De-Equip");

            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems[i].IsEmpty && inventoryItems[i].invType.GetType() == typeof(InventoryItemSO))
                {
                    Debug.Log(i);
                    inventoryItems[i] = new InventoryItem(inventoryItems[slot.index].amount, inventoryItems[slot.index].item, inventoryItems[i].invType, inventoryItems[slot.index].itemState);
                    inventoryItems[slot.index] = new InventoryItem(0, null, inventoryItems[slot.index].invType);
                    break;
                }
            }
        }
        else // auto equip
        {
            if (inventoryItems[slot.index].item.GetType() == typeof(InventoryItemSO)) return;

            Debug.Log("Equip");
            if (inventoryItems[slot.index].item.GetType() == typeof(SecondaryWeaponSO))
            {
                for (int i = inventoryItems.Count -1; i > -1; i--)
                {
                    if (!inventoryItems[i].IsEmpty) continue;

                    if (inventoryItems[i].invType.GetType() == typeof(WeaponSO) || inventoryItems[i].invType.GetType() == typeof(SecondaryWeaponSO))
                    {
                        inventoryItems[i] = new InventoryItem(inventoryItems[slot.index].amount, inventoryItems[slot.index].item, inventoryItems[i].invType, inventoryItems[slot.index].itemState);
                        inventoryItems[slot.index] = new InventoryItem(0, null, inventoryItems[slot.index].invType);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < inventoryItems.Count; i++)
                {
                    if (!inventoryItems[i].IsEmpty) continue;
         

                    if (inventoryItems[slot.index].item.GetType() == inventoryItems[i].invType.GetType())
                    {
                        inventoryItems[i] = new InventoryItem(inventoryItems[slot.index].amount, inventoryItems[slot.index].item, inventoryItems[i].invType, inventoryItems[slot.index].itemState);
                        inventoryItems[slot.index] = new InventoryItem(0, null, inventoryItems[slot.index].invType);
                        break;
                    }

                }
            }
        }

        InformChange();
    }

    public Dictionary<int,InventoryItem> GetCurrentInventoryState()
    {
        Dictionary<int,InventoryItem> returnValue = new Dictionary<int,InventoryItem>();
        for (int i = 0; i < inventoryItems.Count; i++)
        {
            if (inventoryItems[i].IsEmpty) continue;

            returnValue[i] = inventoryItems[i];
        }

        return returnValue;
    }

    public void InformChange()
    {
        OnChange?.Invoke(GetCurrentInventoryState());
    }

    public bool CheckItemExsist(InventoryItemSO inventoryItemSO)
    {
        for(int i =0; i < inventoryItems.Count; i++)
        {
            if (inventoryItems[i].item == inventoryItemSO)
            {
                return true;
            }
        }

        return false;
    }

    public void ModifyParameter(List<ItemParameter> itemCurrentstate, List<ItemParameter> parametertoModify)
    {
        foreach (var parameter in parametertoModify)
        {

            if (itemCurrentstate.Contains(parameter))
            {
                int index = itemCurrentstate.IndexOf(parameter);
                float newValue = itemCurrentstate[index].value + parameter.value;
                itemCurrentstate[index] = new ItemParameter
                {
                    itemParameter = parameter.itemParameter,
                    value = newValue,
                };
            }
        }
    }
}

[Serializable]
public struct InventoryItem
{
    public int amount;
    public InventoryItemSO item;
    public InventoryItemSO invType;
    public List<ItemParameter> itemState;
    public bool IsEmpty => item == null;

    public InventoryItem(int _amount, InventoryItemSO _item, InventoryItemSO _invType,List<ItemParameter> itemParameters = null)
    {
        amount = _amount;
        item = _item;
        itemState = itemParameters;
        invType = _invType;
    }
    public InventoryItem ChangeQuantity(int newQuantity)
    {
        return new InventoryItem
        {
            item = this.item,
            amount = newQuantity,
            itemState = new List<ItemParameter>(this.itemState)
        };
    }

    public ItemParameter GetParameter(string paramName)
    {
        return itemState.Where(x => x.itemParameter.ParameterName == paramName).Single();
    }

    public int GetParameterIndex(string paramName)
    {
        return itemState.IndexOf(GetParameter(paramName));
    }

    public static InventoryItem GetEmptyItem() =>  new InventoryItem {item=null, amount = 0,itemState = null };
}

