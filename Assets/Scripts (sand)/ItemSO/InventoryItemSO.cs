using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName ="InventoryItem",menuName = "Item/Item")]
public class InventoryItemSO : ScriptableObject
{
    public int id => GetInstanceID();
    public string itemName;
    public int amount;
    public Sprite icon;
    public bool stackable;
    public int maxStack;
    public ItemType itemType = ItemType.Item;
    [field:SerializeField]public List<ItemParameter> DefaultParametersList { get; set; }

    public ItemParameter GetParameter(string paramName)
    {
        return DefaultParametersList.Where(x => x.itemParameter.ParameterName == paramName).Single();
    }
}

[Serializable]
public struct ItemParameter : IEquatable<ItemParameter>
{
    public ItemParameterSO itemParameter;
    public float value;

    public ItemParameter(ItemParameterSO itemParameterSO, float value)
    {
        itemParameter = itemParameterSO;
        this.value = value;
    }

    public bool Equals(ItemParameter other)
    {
        return other.itemParameter == itemParameter;
    }
}


public enum ItemType { Throwable,Weapon,WeaponSecondary, Armor, Item}
