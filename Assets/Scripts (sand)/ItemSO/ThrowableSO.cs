using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Item/Throwable")]
public class ThrowableSO : InventoryItemSO
{
    public float Damage;
    public float Range;
    public float Radius;
    public float throwDistance;

}
