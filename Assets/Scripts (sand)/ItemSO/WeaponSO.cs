using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
[CreateAssetMenu(fileName = "Item", menuName = "Item/Weapon")]
public class WeaponSO : InventoryItemSO,IItemAction
{
    public float damage;
    public float Range;
    public float fireRate;
    public int magSize;
    public int currentAmmo;
    public float burstInterval;
    public int burstAmount = 1;
    public InventoryItemSO ammoType;
    public FireMode fireMode;

    public string ActionName => "Equip";

    public AudioClip actionsfx => throw new System.NotImplementedException();

    public bool PerformAction(GameObject character,List<ItemParameter> itemState =null)
    {
        throw new System.NotImplementedException();
    }
}


public enum FireMode { Burst,Single,Toggleable}
