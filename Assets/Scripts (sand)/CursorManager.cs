using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    Texture2D idleCursor;
    private Vector2 offset = new Vector2(64, 64);


    private void OnEnable()
    {
        CursorChanger.OnHover += ChangeCursor;
        CursorChanger.OnExit += ChangeCursorExit;
    }

    private void OnDisable()
    {
        CursorChanger.OnHover -= ChangeCursor;
        CursorChanger.OnExit -= ChangeCursorExit;
    }


    public void ChangeCursor(Texture2D cursorTexture)
    {
        Cursor.SetCursor(cursorTexture, offset, CursorMode.Auto);
    }

    public void ChangeCursorExit()
    {
        Cursor.SetCursor(idleCursor, offset, CursorMode.Auto);
    }

  
}
