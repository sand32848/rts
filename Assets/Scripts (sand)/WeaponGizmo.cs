using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGizmo : MonoBehaviour
{
    [SerializeField] private WeaponSO weaponSO;

    private void OnDrawGizmos()
    {
        if (!weaponSO) return;

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, weaponSO.Range);
    }
}
