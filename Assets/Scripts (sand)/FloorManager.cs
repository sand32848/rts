using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour
{
    [SerializeField] private int currentFloorIndex;

    public static Action<int> OnChangeFloor;

    private void Start()
    {
        OnChangeFloor?.Invoke(currentFloorIndex);
    }

    public void changeFloor(int floorIndex)
    {
        currentFloorIndex = floorIndex;
        OnChangeFloor?.Invoke(currentFloorIndex);
    }

    public void UpFloor()
    {
        currentFloorIndex += 1;
        currentFloorIndex = Mathf.Clamp(currentFloorIndex, 0, 10);
        OnChangeFloor?.Invoke(currentFloorIndex);

    }

    public void DownFloor()
    {
        currentFloorIndex -= 1;
        currentFloorIndex = Mathf.Clamp(currentFloorIndex,0, 10);
        OnChangeFloor?.Invoke(currentFloorIndex);
    }
}
