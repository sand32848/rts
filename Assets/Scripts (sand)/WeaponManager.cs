using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class WeaponManager : MonoBehaviour
{
    [SerializeField] public InventoryItem currentEquip { get; private set; }
    [SerializeField] private List<InventoryItem> weaponList= new List<InventoryItem>();
    private AttackController attackController;
    public static Action<InventoryItem> OnChangeWeapon;

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.WeaponSwitch.started += SwitchWeapon;
        InventoryController.OnWeaponChange += UpdateWeapon;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.WeaponSwitch.started += SwitchWeapon;
        InventoryController.OnWeaponChange -= UpdateWeapon;
    }

    private void Start()
    {
        attackController = GetComponent<AttackController>();
    }

    public void SwitchWeapon(InputAction.CallbackContext context)
    {
        currentEquip = weaponList[(int)context.ReadValue<float>() -1];
        attackController.SetWeapon(currentEquip);
        OnChangeWeapon?.Invoke(currentEquip);
    }

    public void UpdateWeapon(Dictionary<int, InventoryItem> inventoryState)
    {
        foreach (var item in inventoryState)
        {
            if(item.Key == 0) { weaponList[0]  = inventoryState[0]; }
            if (item.Key == 1) { weaponList[1] = inventoryState[1]; }
            if (item.Key == 2) { weaponList[2] = inventoryState[2]; }
            if (item.Key == 4) { weaponList[3] = inventoryState[4]; }

        }
    }
}
