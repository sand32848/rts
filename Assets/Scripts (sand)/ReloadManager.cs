using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ReloadManager : MonoBehaviour
{
    private AttackController attackController;
    public static Action<InventoryItem> OnWeaponReload;

    private void Start()
    {
        attackController = GetComponent<AttackController>();
    }

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Reload.started += RequestReload;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Reload.started -= RequestReload;
    }

    public void RequestReload(InputAction.CallbackContext context)
    {
        WeaponSO weapon = (WeaponSO)attackController.currentWeapon.item;
       int difference = weapon.magSize - weapon.currentAmmo;

        if (difference <= 0) return;

        OnWeaponReload?.Invoke(attackController.currentWeapon);
    }
}
