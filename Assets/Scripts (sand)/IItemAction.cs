using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public interface IItemAction 
{
    public string ActionName { get; }
    public AudioClip actionsfx { get; }
    bool PerformAction(GameObject character,List<ItemParameter> itemState);
    
}
