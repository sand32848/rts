using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorChanger : MonoBehaviour
{
    [SerializeField] private int floorIndex;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Stair") return;

        if (other.transform.TryGetComponent(out FloorObject floorObject))
        {
            if(floorIndex == 0)
            {
                if(floorIndex == floorObject.floorIndex)
                {
                    floorObject.ChangeFloor(1);
                }
                else
                {
                    floorObject.ChangeFloor(0);
                }

            }
            else
            {
                if (floorObject.floorIndex <= floorIndex)
                {
                    floorObject.ChangeFloor(floorObject.floorIndex + 1);
                }
                else
                {
                    floorObject.ChangeFloor(floorObject.floorIndex - 1);
                }
            }

            floorObject.CheckFloor(floorObject.floorIndex);

            if(floorObject.transform.tag == "Player")
            {
                FloorManager.OnChangeFloor(floorObject.floorIndex);
            }
        }
    }
}
