using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePlayerButton : MonoBehaviour
{
    private GameObject player;

    public static Action<GameObject> OnFocusPlayer;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    public void focusPlayer()
    {
        OnFocusPlayer?.Invoke(player);
    }
}
