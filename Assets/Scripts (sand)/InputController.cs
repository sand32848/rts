using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : Singleton<InputController>
{
    private PlayerAction _playerAction;
    public PlayerAction PlayerAction
    {
        get
        {
            if (_playerAction == null)
                _playerAction = new PlayerAction();
            _playerAction.Enable();
            return _playerAction;
        }
    }
 
}
