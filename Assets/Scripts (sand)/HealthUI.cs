using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textMesh;
    private void OnEnable()
    {
        PlayerHealth.OnHealthChange += UpdateHealthUI;
    }

    private void OnDisable()
    {
        PlayerHealth.OnHealthChange -= UpdateHealthUI;

    }

    public void UpdateHealthUI(float currentHealth, float maxHealth)
    {
        textMesh.text = currentHealth.ToString("0");
    }
}
